import csv
from datetime import datetime
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.dates as md


def main():
    delays = []
    timestamps = []
    ref_time = 0
    timestamp = 0
    ref = [0,0]
    with open('./timestamps.csv') as csv_file:
        csv_reader = csv.reader(csv_file,delimiter = ';')
        line_count = 0
        rows = 0
        timer_time = 0.1#sec
        for row in csv_reader:
            if (rows == 0):
                ref[0],ref[1] = row[0],row[1]
                ref_time = float(ref[0])/1.0 + float(ref[1])/1000000
            else:
                sec,usec = row[0],row[1]
                timestamp = float(sec)/1.0 + float(usec)/1000000
                delay = timestamp - (ref_time + timer_time*rows)
                if( abs(delay*1000000)<3000):
                    delays.append(delay*1000000)
                dt_object = datetime.fromtimestamp(timestamp)
                timestamps.append(dt_object)
                print delay*1000000
            rows = rows + 1

    


    plt.rcParams.update({'figure.figsize':(7,5), 'figure.dpi':100})
    plt.hist(delays, bins=500, align='left',density=True)        
    plt.gca().set(title='Frequency Histogram of Delays', ylabel='Frequency', xlabel = 'delays in us');
    plt.show()
    #plt.subplots_adjust(bottom=0.2)
    #plt.xticks( rotation=25 )
    #ax=plt.gca()
    #plt.gca().set(title='deadline delays of interrupt calls', xlabel='timespamp in Greek Format', ylabel = 'delays in us');
    #xfmt = md.DateFormatter('%Y-%m-%d %H:%M:%S')
    #ax.xaxis.set_major_formatter(xfmt)
    #plt.plot(timestamps,delays)
    #plt.show()


if __name__ == "__main__":
    main()