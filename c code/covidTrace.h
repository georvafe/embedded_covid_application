#ifndef COVIDTRACE_H
#define COVIDTRACE_H

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include "queue.h"
#define N 1000

FILE *timespampsFile;
FILE *informerFile;

struct timeval callTimespamp,executeTimespamp,callDifference, refferenceTime;

float minForCloseContact = 240,maxForCloseContact=1200;//seconds
int NumberOfContacts = 0;
typedef struct macaddresses{
    uint8_t addr[6];
}macaddress;
macaddress *MACs;//all the MACs in the world


typedef struct {
    macaddress addr;
    queue *QueueOfTimestamps;
}registry;//used for temporal contacts

typedef struct{
    int isClose ;
    macaddress contactAddr;
    struct timeval lastUpdated;

}contact;//used for contacts that have been existed as Close Contacts or they are still Close Contacts
contact ListOfCloseContacts[N];//Close contacts
void initCloseContacts(void){
    int i,k;
    for(i = 0 ; i<N;i++){
        ListOfCloseContacts[i].isClose = 0;
        for(k = 0 ; k<6;k++)
        ListOfCloseContacts[i].contactAddr.addr[k] = MACs[i].addr[k];
    }
}
registry possibleContacts[N];
int lastPossibleContactNum = 0;
int MacEqual(macaddress addr1,macaddress addr2){//check if 2 contacts has the same MAC address 
    int i = 0;
    for(i = 0;i<6;i++){
        if(addr1.addr[i] !=addr2.addr[i])return 0;
    }
    return 1;
}

void UpdatePossibleContacts(void){//dequeue timstamps that have been expired because 20min passed
    int i = 0;
    struct timeval presentTime;
    gettimeofday(&presentTime,NULL);
    //timersub(struct timeval *a, struct timeval *b,struct timeval *res);
    for(i= 0 ; i<lastPossibleContactNum;i++){//for each contact in Possible contacts
                                            //check if must included in close contacts
        if(!isEmpty(possibleContacts[i].QueueOfTimestamps)){
            long oldest = possibleContacts[i].QueueOfTimestamps->head;
            long newer = possibleContacts[i].QueueOfTimestamps->tail;
            struct timeval oldestRegistry = possibleContacts[i].QueueOfTimestamps->buf[oldest];
            struct timeval newerRegistry = possibleContacts[i].QueueOfTimestamps->buf[newer];
            struct timeval diff;
            timersub(&newerRegistry,&oldestRegistry,&diff);
            if(diff.tv_sec>2 && diff.tv_usec>400000 && diff.tv_sec<12){//it is close contact
                
                int j = 0;
                
                for(j = 0;j<N;j++){
                    if(MacEqual(possibleContacts[i].addr,ListOfCloseContacts[j].contactAddr)){//check in ListOfCloseContacts if already exists
                        ListOfCloseContacts[j].isClose = 1;
                        ListOfCloseContacts[j].lastUpdated = newerRegistry;
                    }
                }
            }
            //check if it have expired timestamps on head
            struct timeval diffpr;
            timersub(&newerRegistry,&oldestRegistry,&diffpr);
            if(diffpr.tv_sec>12){
                deQueue(possibleContacts[i].QueueOfTimestamps);
            }
        }

    }

}
void importInPossibleContacts(macaddress possibleContact){//save temporally the contacts and theirs timestamps in order to fill the queue of timestamps
                                                          // this contacts we be saved in ListOfCloseContacts if they have time difference of 4-20min
    //Check if exists
    int i = 0;
    int exists = 0;
    struct timeval tv;
    gettimeofday(&tv,NULL);
    for(i=0;i<lastPossibleContactNum;i++){
        if(MacEqual(possibleContact,possibleContacts[i].addr)==1){
            exists = 1;
            enQueue(possibleContacts[i].QueueOfTimestamps,tv);
        }
    }
    if(exists==0){//import new registry
        possibleContacts[lastPossibleContactNum].addr = possibleContact;
        possibleContacts[lastPossibleContactNum].QueueOfTimestamps = queueInit();
        enQueue(possibleContacts[lastPossibleContactNum].QueueOfTimestamps,tv);

        lastPossibleContactNum++;
    }

}
void uploadContacts(void){//we append the inform.txt log file every time that we have a possitive covid test in the interrupt handler
    informerFile = fopen("./inform.txt", "a+");
    int i = 0 ;
    int countOfclose=0;
    struct timeval present;
    struct timeval diff;
    struct timeval day;
    gettimeofday(&present,NULL);
    timersub(&present,&refferenceTime,&day);
    //printing the timestamp from the beginning of the experiment
    fprintf(informerFile, "Positive Covid test at %ld: \n",day.tv_sec);
    for(i = 0 ; i<N;i++){
        if(ListOfCloseContacts[i].isClose==1){//check only for contacts that have been recently change in Close Contact
            
            timersub(&present,&ListOfCloseContacts[i].lastUpdated,&diff);
            if(diff.tv_sec>12096){//check if the duration is more than 14 days and dont inform them.Also update their situation in non-Close
                ListOfCloseContacts[i].isClose = 0;
            }
            else{
                 
                fprintf(informerFile, "the mac address of the %dth contact is:",countOfclose);
                int k;
                for( k=0 ;k<6;k++){//this is the print of MAC address
                   
                    fprintf(informerFile, "%d",ListOfCloseContacts[i].contactAddr.addr[k]);
                }
                
                fprintf(informerFile, " and time diff is %ld\n",diff.tv_sec);
                countOfclose++;
                
            }
        }
    }
    fclose(informerFile);
}

macaddress BTNearMe(){//return a random MAC from the list of MACs that i created
    int randomizer = 0 ;
    randomizer = rand()%N;
    return MACs[randomizer];
}
int testCovid(){
    int randomizer = 0;
    randomizer = rand()%101+1;
    if(randomizer>60){// returns true with possibility 40%
        return 1;
    }
    else{
        return 0;
    }
}

void createRandomMAC(macaddress *macAddress){
    int i;
    for(i = 0; i<6;i++){
        macAddress->addr[i] = rand()%9;
    }

}

void createListOfMAC(macaddress *macAddress,int population){
    
    int i= 0;
    for(i=0;i<population;i++){
        createRandomMAC(&macAddress[i]);
    }
}

void printListOfMACs(void){
    int i,j;
    for(i = 0 ; i <N ; i++){
        printf("the %d is :\n",i);
        for(j = 0 ; j<6;j++){
            printf("%d",ListOfCloseContacts[i].contactAddr.addr[j]);
            
        }
        printf("\n");
    }
}
#endif