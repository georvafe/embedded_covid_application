#include "covidTrace.h"
#include <sys/time.h>
#include <signal.h>
#include <unistd.h>
#include <math.h>
#include <string.h>


int experimentTIme = 259200;//30*24*60*60 / 10;//this is the number of times that the loop is about to run
int count = 0;
int firstTime = 0;
struct timeval tv;



void handler(int sig){
    gettimeofday(&executeTimespamp,NULL);
    fprintf(timespampsFile, "%ld;%ld\n",executeTimespamp.tv_sec,executeTimespamp.tv_usec);
    struct timeval before,after,diff;
    if(firstTime==0){
        firstTime = 1;
        gettimeofday(&refferenceTime,NULL);
        
    }
    
    if(count %9000==0){//close every 15 minutes for safety reasons
        fclose(timespampsFile);
        timespampsFile = fopen("./timestamps.csv", "a+");

    }
    gettimeofday(&before,NULL);
     importInPossibleContacts(BTNearMe());
     UpdatePossibleContacts();
     gettimeofday(&after,NULL);
      timersub(&after,&before,&diff);
      if(count%1440 == 0){
          if(testCovid() == 1){
                printf("\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t self test is true\n");
                uploadContacts();
            }
            else printf("\n\t\t\t\t\t\t\t\t\t\t\t\t\t\t self test is false\n");
      }
      printf("\t\t\t\t\t\t lasted seconds : %ld\tmicro seconds : %ld\n",diff.tv_sec, diff.tv_usec);
}



struct timeval transTime(double time){//i transform double to struct timeval for use in timer and sleeps
    struct timeval delay;
    double fl_time;
    fl_time = floor(time);

    delay.tv_sec = (int)fl_time;
    delay.tv_usec = (int)(1e6*(time-fl_time));
    printf("%lf ,%lf , %lf\n",time,fl_time,time-fl_time);
    return delay;
}


void main(int argc,char **argv){
    double D = 0.1;
    int i = 0,j;
    struct timeval beg,stop,differ;


    timespampsFile = fopen("./timestamps.csv", "w+");
    
    
    informerFile = fopen("./inform.txt", "w+");
    fclose(informerFile);
    // variables for the itimer
    struct sigaction sa;
    struct itimerval timer;
    //the time that counts
    timer.it_value = transTime(D);//timer.it_value include a struct of timeval
    timer.it_interval = transTime(D);//timer.it_internal include a struct of timeval

    

    //init the timer
    memset(&sa, 0, sizeof(sa));
    sa.sa_handler = &handler;
    sigaction(SIGALRM, &sa, NULL);

    

    //begin experiment
    setitimer(ITIMER_REAL, &timer, NULL);
   
    
    int SelfTestTime = 4*60*60/10;//This is the period that we make self-test.
    
    MACs = (macaddress *)malloc(N*sizeof(macaddress));
    createListOfMAC(MACs,N);
    
    initCloseContacts();
    gettimeofday(&beg,NULL);
    while(count++ < experimentTIme){
        //every 10 second i seek for BT macs
        
         
        
        //every 10 seconds i update the possible close contacts depending to the duration from their timestamps.
        //We might need to deQueue some old timestamps that are more than 20 min

        //every 10 seconds i update the ListOfContacts in case that 14 days passed

        
        sleep(52);
    }
    gettimeofday(&stop,NULL);
    timersub(&stop,&beg,&differ);
    //stop the clock
    timer.it_value.tv_sec = 0;
    timer.it_value.tv_usec = 0;
    timer.it_interval.tv_sec = 0;
    timer.it_interval.tv_usec = 0;
    setitimer(ITIMER_REAL,&timer,NULL);
    

     printf("\n total elapse time seconds : %ld micro seconds : %ld \n",
    differ.tv_sec, differ.tv_usec);
    fclose(timespampsFile);
    free(MACs);
}