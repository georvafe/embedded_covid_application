#ifndef QUEUE_H
#define QUEUE_H

#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#define QUEUESIZE 120

typedef struct{
    struct timeval buf[QUEUESIZE];
    long head, tail;
    int full, empty;
}queue;



queue *queueInit (void)
{
  queue *q;

  q = (queue *)malloc (sizeof (queue));
  if (q == NULL) return (NULL);

  q->empty = 1;
  q->full = 0;
  q->head = -1;
  q->tail = -1;
  
	
  return (q);
}

void queueDelete (queue *q)
{
  free (q);
}

void queueUpdate(queue *q);


int isFull(queue *q) {
  if ((q->head == q->tail + 1) || (q->head == 0 && q->tail == QUEUESIZE - 1)) return 1;
  return 0;
}

// Check if the queue is empty
int isEmpty(queue *q) {
  if (q->head == -1) return 1;
  return 0;
}

// Adding an element
void enQueue(queue *q,struct timeval element) {
  if (isFull(q))
    printf("\n Queue is full!! \n");
  else {
    if (q->head == -1) q->head = 0;
    q->tail = (q->tail + 1) % QUEUESIZE;
    q->buf[ q->tail] = element;
   printf("seconds : %ld\tmicro seconds : %ld,head:  %ld,tail: %ld\n",
    element.tv_sec, element.tv_usec,q->head,q->tail);
  }
}

// Removing an element
struct timeval deQueue(queue *q) {
  struct timeval element;
  if (isEmpty(q)) {
    printf("\n Queue is empty !! \n");
    return (element);
  } else {
    element = q->buf[q->head];
    if (q->head == q->tail) {
      q->head = -1;
      q->tail = -1;
    } 
    // Q has only one element, so we reset the 
    // queue after dequeing it. ?
    else {
      q->head = (q->head + 1) % QUEUESIZE;
    }
    printf("seconds : %ld\nmicro seconds : %ld",
    element.tv_sec, element.tv_usec);
    return (element);
  }
}
#endif